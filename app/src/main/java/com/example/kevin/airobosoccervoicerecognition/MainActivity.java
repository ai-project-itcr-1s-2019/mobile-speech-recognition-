package com.example.kevin.airobosoccervoicerecognition;

import android.bluetooth.BluetoothAdapter;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

public class MainActivity extends AppCompatActivity implements RecognitionListener {

    private static final String KWS_SEARCH = "wakeup";
    private static final String MENU_SEARCH = "menu";

    private static String FORWARD = "forward";
    private static String BACK = "back";
    private static String LEFT = "left";
    private static String RIGHT = "right";
    private static String TURN = "turn";
    private static String SCORE = "score";

    private Button refreshButton;
    private ListView devicesList;
    private TextView text;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayAdapter<String> arrayAdapter;
    List<String> dispositivos = new ArrayList<String>();


    /* Keyword we are looking for to activate recognition */
    private static final String KEYPHRASE = "gideon";

    /* Recognition object */
    private SpeechRecognizer recognizer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        runRecognizerSetup();
    }

    private void runRecognizerSetup() {
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(MainActivity.this);
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                if (result != null) {
                    System.out.println(result.getMessage());
                } else {
                    switchSearch(KWS_SEARCH);
                }
            }
        }.execute();
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                // Disable this line if you don't want recognizer to save raw
                // audio files to app's storage
                //.setRawLogDir(assetsDir)
                .getRecognizer();
        recognizer.addListener(this);
        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);
        // Create your custom grammar-based search
        File menuGrammar = new File(assetsDir, "mymenu.gram");
        recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;
        String text = hypothesis.getHypstr();
        /*if (text.equals(KEYPHRASE))
            switchSearch(MENU_SEARCH);
        else {
            System.out.println(hypothesis.getHypstr());
        }*/

        if (text.equals(KEYPHRASE)){
            Toast.makeText(this, "you just said our secret keyword!!", Toast.LENGTH_SHORT).show();
            switchSearch(MENU_SEARCH);

        } else if (text.equals(FORWARD)) {
            System.out.println(FORWARD);
            Toast.makeText(this, FORWARD, Toast.LENGTH_SHORT).show();

        } else if (text.equals(BACK)) {
            System.out.println(BACK);
            Toast.makeText(this, BACK, Toast.LENGTH_SHORT).show();

        }else if (text.equals(LEFT)) {
            System.out.println(LEFT);
            Toast.makeText(this, LEFT, Toast.LENGTH_SHORT).show();
        }
        else if (text.equals(RIGHT)) {
            System.out.println(RIGHT);
            Toast.makeText(this, RIGHT, Toast.LENGTH_SHORT).show();
        }
        else if (text.equals(TURN)) {
            System.out.println(TURN);
            Toast.makeText(this, TURN, Toast.LENGTH_SHORT).show();
        }
        else if (text.equals(SCORE)) {
            System.out.println(SCORE);
            Toast.makeText(this, SCORE, Toast.LENGTH_SHORT).show();
        }
        else {
            System.out.println(hypothesis.getHypstr());
            Toast.makeText(this, "HYPHOTESIS: "+hypothesis.getHypstr(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResult(Hypothesis hypothesis) {
        if (hypothesis != null) {
            System.out.println("hola, "+hypothesis.getHypstr());
        }
    }

    @Override
    public void onBeginningOfSpeech() {
    }

    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 5000);
    }

    @Override
    public void onError(Exception error) {
        System.out.println(error.getMessage());
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
